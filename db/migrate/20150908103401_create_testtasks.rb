class CreateTesttasks < ActiveRecord::Migration
  def change
    create_table :testtasks do |t|
      t.string :name
      t.string :tasktext
      t.datetime :created
      t.datetime :updated

      t.timestamps null: false
    end
  end
end
