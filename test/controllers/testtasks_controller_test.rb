require 'test_helper'

class TesttasksControllerTest < ActionController::TestCase
  setup do
    @testtask = testtasks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:testtasks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create testtask" do
    assert_difference('Testtask.count') do
      post :create, testtask: { created: @testtask.created, name: @testtask.name, tasktext: @testtask.tasktext, updated: @testtask.updated }
    end

    assert_redirected_to testtask_path(assigns(:testtask))
  end

  test "should show testtask" do
    get :show, id: @testtask
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @testtask
    assert_response :success
  end

  test "should update testtask" do
    patch :update, id: @testtask, testtask: { created: @testtask.created, name: @testtask.name, tasktext: @testtask.tasktext, updated: @testtask.updated }
    assert_redirected_to testtask_path(assigns(:testtask))
  end

  test "should destroy testtask" do
    assert_difference('Testtask.count', -1) do
      delete :destroy, id: @testtask
    end

    assert_redirected_to testtasks_path
  end
end
