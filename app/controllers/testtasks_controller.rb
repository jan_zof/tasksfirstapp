class TesttasksController < ApplicationController
  before_action :set_testtask, only: [:show, :edit, :update, :destroy]

  # GET /testtasks
  # GET /testtasks.json
  def index
    @testtasks = Testtask.all
  end

  # GET /testtasks/1
  # GET /testtasks/1.json
  def show
  end

  # GET /testtasks/new
  def new
    @testtask = Testtask.new
  end

  # GET /testtasks/1/edit
  def edit
  end

  # POST /testtasks
  # POST /testtasks.json
  def create
    @testtask = Testtask.new(testtask_params)

    respond_to do |format|
      if @testtask.save
        format.html { redirect_to @testtask, notice: 'Testtask was successfully created.' }
        format.json { render :show, status: :created, location: @testtask }
      else
        format.html { render :new }
        format.json { render json: @testtask.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /testtasks/1
  # PATCH/PUT /testtasks/1.json
  def update
    respond_to do |format|
      if @testtask.update(testtask_params)
        format.html { redirect_to @testtask, notice: 'Testtask was successfully updated.' }
        format.json { render :show, status: :ok, location: @testtask }
      else
        format.html { render :edit }
        format.json { render json: @testtask.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /testtasks/1
  # DELETE /testtasks/1.json
  def destroy
    @testtask.destroy
    respond_to do |format|
      format.html { redirect_to testtasks_url, notice: 'Testtask was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_testtask
      @testtask = Testtask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def testtask_params
      params.require(:testtask).permit(:name, :tasktext, :created, :updated)
    end

  #
  #
  # #comments
  # # POST /comments
  # # POST /comments.json
  # def createComment
  #   @comment = Comment.new(comment_params)
  #
  #   respond_to do |format|
  #     if @comment.save
  #       format.html { redirect_to @comment, notice: 'Comment was successfully created.' }
  #       format.json { render :show, status: :created, location: @comment }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @comment.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end
  #
  # # Never trust parameters from the scary internet, only allow the white list through.
  # def comment_params
  #   params.require(:comment).permit(:title, :comment, :testtask_id)
  # end
end
