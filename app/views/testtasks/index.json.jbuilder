json.array!(@testtasks) do |testtask|
  json.extract! testtask, :id, :name, :tasktext, :created, :updated
  json.url testtask_url(testtask, format: :json)
end
